/*accordion*/
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("active");

    var panel = this.nextElementSibling;
    if (panel.style.display === "flex") {
      panel.style.display = "none";
      this.querySelector('.caret').style.transform = "rotate(0deg)";
    } else {
      panel.style.display = "flex";
      this.querySelector('.caret').style.transform = "rotate(-180deg)";
    }

  });
}

/*like counter*/
var clicks = 0;
function onClick() {
    clicks += 1;
    document.getElementById("clicks").innerHTML = clicks;
};

/*mail to new tab*/
function sendEmail(){
  const windowRef = window.open(`mailto:macdevh@gmail.com`, '_blank');
  
  windowRef.focus();
  
  setTimeout(function(){
    if(!windowRef.document.hasFocus()){
      windowRef.close();
    }
  }, 500);
}