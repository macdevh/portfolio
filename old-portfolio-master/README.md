# New Portfolio - Main Site and Content Complete. To Do List are Minor Changes

## By Mac

### To Do List
    * make whole of technology card clickable
    * make hearts clickable with like counter
    * make either blog or tech cards flippable not both possibly hover window for tech
    * ~~add analytics~~
    * look into kubernetes
    * ~~404 Page~~
#### Content - COMPLETE
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer


#### Layouts Mobile
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer
#### Layouts Mobile Landscape
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer
#### Layouts Tablet
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer
#### Layouts Tablet Landscape
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer
#### Layouts Desktop
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer

#### Custom Stylings
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer

#### Replace Placeholders With Content
- [X] Home
- [X] Services
- [X] Technologies
- [X] Portfolio
- [X] Blog
- [X] Footer


#### Other Ideas
- [ ] Recreate logo with better technologies
- [ ] Add stroke to logo
- [ ] Try Logo with computer code or similar on the darker side


##### Other Information
repo: https://gitlab.com/macdevh/my-portfolio

twitter: [@Mac_Hooper](https://twitter.com/mac_hooper)

color pallete: https://flatuicolors.com/palette/gb

blog post thumb size: 232 x 220
blog post hero size: 1280 x 720

React Framework Technology removed to add inside of web. Modal code is still in a folder in docs in case mind is changed.